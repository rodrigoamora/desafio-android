package br.com.desafioandroid.shared.model.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Util {

	private static final String LOG_TAG = "Class Util";
	private static ProgressDialog progressDialog;
	
	public static void showProgressDiaolg(Context ctx, String title, String msg, boolean cancelable) {
		progressDialog = ProgressDialog.show(ctx, title, msg, false, cancelable);
	}
	
	public static void setTitleProgressDialog(String msg) {
		progressDialog.setMessage(msg);
	}
			
	public static void dimissProgressDialog() {
		progressDialog.dismiss();
	}
	
	public static void showToast(Context ctx, String msg) {
		Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
	}
	
	public static boolean checkConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean mobileIsConnected = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
        boolean wifiIsConnected = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();

        if (!mobileIsConnected && !wifiIsConnected) {
            return false;
        }
        
        return true;
    }
	
	public static void hiddenKeyBoard(Activity activity, Context context, View view) {
    	InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
	
	public static PackageInfo getVersionApp(Activity activity) throws NameNotFoundException {
		return activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager	= (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public static boolean hasActiveInternetConnection(Context context) {
		if (isNetworkAvailable(context)) {
			try {
				HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.boxes-list.com").openConnection());
				urlc.setRequestProperty("User-Agent", "Test");
				urlc.setRequestProperty("Connection", "close");
				urlc.setConnectTimeout(1500);
				urlc.connect();
				return (urlc.getResponseCode() == 200);
			} catch (IOException e) {
				Log.e(LOG_TAG, "Error checking internet connection", e);
			}
		} else {
			Log.d(LOG_TAG, "No network available!");
		}
		return false;
	}

}
