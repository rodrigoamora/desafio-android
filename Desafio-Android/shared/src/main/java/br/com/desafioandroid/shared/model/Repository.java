package br.com.desafioandroid.shared.model;

import java.util.List;

public class Repository {

    private List<Item> items;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
