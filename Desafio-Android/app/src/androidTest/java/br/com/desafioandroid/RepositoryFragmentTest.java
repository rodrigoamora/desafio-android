package br.com.desafioandroid;

import android.support.v7.widget.RecyclerView;
import android.test.ActivityInstrumentationTestCase2;

import br.com.desafioandroid.fragment.RepositoryFragment;


public class RepositoryFragmentTest extends ActivityInstrumentationTestCase2 {

    public RepositoryFragmentTest() {
        super("br.com.desafioandroid", MainActivity.class);
    }

    public void testRecyclerViewSize() {
        MainActivity activity = (MainActivity) getActivity();
        RepositoryFragment repositoryFragment = (RepositoryFragment) activity.getFragmentManager().findFragmentById(R.id.fragment);

        RecyclerView recyclerView = (RecyclerView) activity.findViewById(R.id.recycler_view);
        
        assertEquals(30, recyclerView.getAdapter().getItemCount());
        assertNotNull(repositoryFragment.repositories.getItems().get(0));
    }

}
