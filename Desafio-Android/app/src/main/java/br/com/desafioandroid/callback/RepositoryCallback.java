package br.com.desafioandroid.callback;

import br.com.desafioandroid.shared.model.Repository;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RepositoryCallback {

    @GET("/search/repositories")
    public Call<Repository> listRespositories(@Query("q")String language, @Query("sort")String sort, @Query("page") int page);

}
