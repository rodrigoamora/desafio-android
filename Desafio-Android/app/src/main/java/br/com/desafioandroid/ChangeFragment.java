package br.com.desafioandroid;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;

public enum ChangeFragment {

    LIST_REPOSITORIES {
        @Override
        public Fragment changeFragment(int id, Class<? extends Fragment> fragmentClass, FragmentManager fragmentManager, boolean backstack, Object... params) {
            Fragment naTela = fragmentManager.findFragmentByTag(fragmentClass.getName());
            if(naTela != null) {
                return naTela;
            }

            try {
                Fragment newFragment = fragmentClass.newInstance();
                newFragment.setArguments(getBundle(params));

                fragmentManager.beginTransaction().addToBackStack(newFragment.getClass().getName()).replace(id, newFragment).commit();
                return newFragment;
            } catch (InstantiationException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected Bundle getBundle(Object... params) {
            return null;
        }
    },
    PULL_REQUESTS {
        @Override
        public Fragment changeFragment(int id, Class<? extends Fragment> fragmentClass, FragmentManager fragmentManager, boolean backstack, Object... params) {
            Fragment naTela = fragmentManager.findFragmentByTag(fragmentClass.getName());
            if(naTela != null) {
                return naTela;
            }

            try {
                Fragment newFragment = fragmentClass.newInstance();
                newFragment.setArguments(getBundle(params));

                fragmentManager.beginTransaction().addToBackStack(newFragment.getClass().getName()).replace(id, newFragment).commit();
                return newFragment;
            } catch (InstantiationException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected Bundle getBundle(Object... params) {
            Bundle bundle = new Bundle();
            bundle.putString("owner", params[0].toString());
            bundle.putString("name", params[1].toString());
            return bundle;
        }
    };

    public abstract Fragment changeFragment(int id, Class<? extends Fragment> fragmentClass, FragmentManager fragmentManager, boolean backstack, Object... params);
    protected abstract Bundle getBundle(Object... params);

}
