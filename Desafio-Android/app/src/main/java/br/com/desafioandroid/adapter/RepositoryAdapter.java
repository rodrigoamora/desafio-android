package br.com.desafioandroid.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.desafioandroid.ChangeFragment;
import br.com.desafioandroid.R;
import br.com.desafioandroid.fragment.PullRequestFragment;
import br.com.desafioandroid.shared.model.Repository;
import de.hdodenhof.circleimageview.CircleImageView;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder> {

    private Activity context;
    private Repository repository;

    public RepositoryAdapter(Activity context, Repository repository) {
        this.context = context;
        this.repository = repository;
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_repository, parent, false);
        RepositoryViewHolder holder = new RepositoryViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder holder, final int position) {
        Picasso.with(context).load(repository.getItems().get(position).getOwner().getAvatar_url()).into(holder.avatar);
        holder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeFragment.PULL_REQUESTS.changeFragment(R.id.fragment, PullRequestFragment.class, context.getFragmentManager(), false, repository.getItems().get(position).getOwner().getLogin(), repository.getItems().get(position).getName());
            }
        });

        holder.titleRepository.setText(repository.getItems().get(position).getName());
        holder.titleRepository.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeFragment.PULL_REQUESTS.changeFragment(R.id.fragment, PullRequestFragment.class, context.getFragmentManager(), false, repository.getItems().get(position).getOwner().getLogin(), repository.getItems().get(position).getName());
            }
        });

        holder.descrpition.setText(repository.getItems().get(position).getDescription());
        holder.nameOwner.setText(repository.getItems().get(position).getOwner().getLogin());
        holder.fullNameOwner.setText(repository.getItems().get(position).getFull_name());
    }

    @Override
    public int getItemCount() {
        return repository.getItems().size();
    }

    public class RepositoryViewHolder extends RecyclerView.ViewHolder {

        CircleImageView avatar;
        TextView titleRepository, descrpition, nameOwner, fullNameOwner;

        public RepositoryViewHolder(View itemView) {
            super(itemView);
            titleRepository = (TextView) itemView.findViewById(R.id.name_repository);
            descrpition = (TextView) itemView.findViewById(R.id.description);
            nameOwner = (TextView) itemView.findViewById(R.id.username);
            fullNameOwner = (TextView) itemView.findViewById(R.id.name);

            avatar = (CircleImageView) itemView.findViewById(R.id.avatar);
        }
    }

}
