package br.com.desafioandroid.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.desafioandroid.R;
import br.com.desafioandroid.shared.model.PullRequest;
import de.hdodenhof.circleimageview.CircleImageView;

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PullRequestViewHolder> {

    private Activity context;
    private List<PullRequest> pullRequests;

    public PullRequestAdapter(Activity context, List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
        this.context = context;
    }

    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_pullrequest, parent, false);
        PullRequestViewHolder holder = new PullRequestViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(PullRequestAdapter.PullRequestViewHolder holder, int position) {
        Picasso.with(context).load(pullRequests.get(position).getUser().getAvatar_url()).into(holder.avatar);

        holder.titlePullRequest.setText(pullRequests.get(position).getTitle());
        holder.description.setText(pullRequests.get(position).getBody());
        holder.fullName.setText(pullRequests.get(position).getUser().getFull_name());
        holder.username.setText(pullRequests.get(position).getUser().getLogin());
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public class PullRequestViewHolder extends RecyclerView.ViewHolder {

        TextView titlePullRequest, username, fullName, description;
        CircleImageView avatar;

        public PullRequestViewHolder(View itemView) {
            super(itemView);

            titlePullRequest = (TextView) itemView.findViewById(R.id.title_pullrequest);
            username = (TextView) itemView.findViewById(R.id.username);
            fullName = (TextView) itemView.findViewById(R.id.full_name);
            description = (TextView) itemView.findViewById(R.id.description);

            avatar = (CircleImageView) itemView.findViewById(R.id.avatar);
        }
    }

}
