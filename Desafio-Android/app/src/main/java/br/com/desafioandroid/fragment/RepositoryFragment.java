package br.com.desafioandroid.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.desafioandroid.R;
import br.com.desafioandroid.adapter.RepositoryAdapter;
import br.com.desafioandroid.callback.RepositoryCallback;
import br.com.desafioandroid.listener.PaginateListener;
import br.com.desafioandroid.shared.model.Repository;
import br.com.desafioandroid.shared.model.util.UrlUtil;
import br.com.desafioandroid.shared.model.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RepositoryFragment extends Fragment {

    public Repository repositories;
    private RepositoryAdapter adapter;
    private RecyclerView recyclerView;

    private int page;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_repository, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        page = 1;

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayoutManager linearLayout = new LinearLayoutManager(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setOnScrollListener(new PaginateListener(linearLayout) {
            @Override
            public void onLoadMore(int current_page) {
                page++;
                loadRepositories();
            }
        });

        repositories = new Repository();
        loadRepositories();
    }

    private void loadRepositories() {
        if (!Util.checkConnection(getActivity())) {
            Snackbar.make(recyclerView, getString(R.string.error_load_repository), Snackbar.LENGTH_LONG).show();
        } else {
            Util.showProgressDiaolg(getActivity(), getString(R.string.loading_repositories), getString(R.string.wait), false);

            GsonConverterFactory f = GsonConverterFactory.create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(UrlUtil.urlBase)
                    .addConverterFactory(f)
                    .build();

            final RepositoryCallback service = retrofit.create(RepositoryCallback.class);
            Call<Repository> call = service.listRespositories("java", "stars", page);
            call.enqueue(new Callback<Repository>() {
                @Override
                public void onResponse(Call<Repository> call, Response<Repository> response) {
                    if (response.body() == null) {
                        Snackbar.make(recyclerView, getString(R.string.error_load_repository), Snackbar.LENGTH_LONG).show();
                    } else {
                        if (repositories.getItems() == null || repositories.getItems().isEmpty()) {
                            repositories.setItems(response.body().getItems());
                        } else {
                            repositories.getItems().addAll(response.body().getItems());
                        }
                        populateRecycleView();
                    }
                    Util.dimissProgressDialog();
                }

                @Override
                public void onFailure(Call<Repository> call, Throwable t) {
                    Snackbar.make(recyclerView, getString(R.string.error_load_repository), Snackbar.LENGTH_LONG).show();
                    Util.dimissProgressDialog();
                }
            });
        }
    }

    private void populateRecycleView() {
        if (page == 1) {
            adapter = new RepositoryAdapter(getActivity(), repositories);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

}
