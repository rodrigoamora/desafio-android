package br.com.desafioandroid.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.desafioandroid.MainActivity;
import br.com.desafioandroid.R;
import br.com.desafioandroid.adapter.PullRequestAdapter;
import br.com.desafioandroid.callback.PullRequestCallback;
import br.com.desafioandroid.shared.model.PullRequest;
import br.com.desafioandroid.shared.model.util.UrlUtil;
import br.com.desafioandroid.shared.model.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PullRequestFragment extends Fragment {

    private List<PullRequest> pullRequests;
    private RecyclerView recyclerView;

    private String name, owner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pullrequest, container, false);

        name = getArguments().getString("name");
        owner = getArguments().getString("owner");

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        MainActivity activity = (MainActivity) getActivity();
        activity.getSupportActionBar().setTitle(name);

        pullRequests = new ArrayList<PullRequest>();

        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getPullRequests();
    }

    private void getPullRequests() {
        if (!Util.checkConnection(getActivity())) {
            Snackbar.make(recyclerView, getString(R.string.alert_no_internet), Snackbar.LENGTH_LONG).show();
        } else {
            Util.showProgressDiaolg(getActivity(), getString(R.string.loading_pullrequest), getString(R.string.wait), false);

            GsonConverterFactory f = GsonConverterFactory.create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(UrlUtil.urlBase)
                    .addConverterFactory(f)
                    .build();

            final PullRequestCallback service = retrofit.create(PullRequestCallback.class);
            Call<List<PullRequest>> call = service.getPullRequests(owner, name);
            call.enqueue(new Callback<List<PullRequest>>() {
                @Override
                public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                    if (response.body() == null) {
                        Snackbar.make(recyclerView, getString(R.string.error_load_pull_request), Snackbar.LENGTH_LONG).show();
                    } else {
                        if (response.body() == null || response.body().isEmpty()) {
                            Snackbar.make(recyclerView, getString(R.string.error_load_pull_request), Snackbar.LENGTH_LONG).show();
                        } else {
                            pullRequests.addAll(response.body());
                            populateRecycleView();
                        }
                    }
                    Util.dimissProgressDialog();
                }

                @Override
                public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                    Snackbar.make(recyclerView, getString(R.string.error_load_pull_request), Snackbar.LENGTH_LONG).show();
                    Util.dimissProgressDialog();
                }
            });
        }
    }


    private void populateRecycleView() {
        PullRequestAdapter adapter = new PullRequestAdapter(getActivity(), pullRequests);
        recyclerView.setAdapter(adapter);
    }

}
