package br.com.desafioandroid.callback;

import java.util.List;

import br.com.desafioandroid.shared.model.PullRequest;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PullRequestCallback {

    @GET("repos/{owner}/{repo}/pulls")
    public Call<List<PullRequest>> getPullRequests(@Path("owner")String owner, @Path("repo")String repo);

}
